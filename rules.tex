\chapter{Rules}

\section{Encounters}

The coterie want to drain blood, covet ghouls, and make deals for hunting grounds.
But their attempts are constantly set back by unwelcome encounters.
Encounters form the blood of the story, sweeping the characters along.

Each story is made from a series of `\textit{encounters}', and each encounter waits for them in a particular area.

The areas here are the \textit{Villages}, \textit{Belgrade}, \textit{Kalemegdan}, \textit{Zemun}, and \textit{War Island}.
A story's first encounter might start in the Villages, the second part might be in Kalemegdan, and the third in Zemun.
The coterie should never be pushed into the next area -- they must find it for themselves.
If the party remain in the Villages, then they get the next story which starts in the Villages.
That story in turn will either have some second encounter in the Villages, or a second part in town.
If an encounter is available from an active story, then that is the encounter they receive.
New stories only begin when no old stories are available to start.

These encounters, played over a long period of time, form a cohesive whole -- or at least a patchwork story.
At the start, wandering here and there will have the party embroiled in multiple arcs simultaneously -- perhaps so many that they will struggle to understand what it happening around them.
However, they will soon find patterns in the mess, as they find more and more encounters relating to previous materials.

Storytellers should also make sure the coterie feel proper reason to move.
They may have their own goals, such as ghouling someone in a particular location, or spying on another cainite.
And then of course the Beast always draws them out, and will inevitably draw them away from their havens.

\subsection{Time Flies}

Encounters are typically focussed around either cainites or mortals.
Those focussed around mortals have to bind the group to the current decade -- if they begin a story with a mortal in the year 1230, they cannot progress through many decades.
Once such a plot ends, however, you should feel free to begin a lengthy downtime, perhaps forty or fifty years.
Plots involving other cainites have no time constraints.
If they annoy Clara Zantosa in 1230, she will remember the grudge two hundred years later.

If you want to see a campaign stretched out over some centuries, you may want to entertain only one story involving mortals at a time, so that the coterie can make the appropriate leaps through the centuries.

\section{Side Character Deaths}

Sometimes characters may die before you expect them to.
Perhaps the hunter kills the twins, or perhaps the characters take out Mihailo and successfully cover up their crimes.
This can make the remainders of encounters difficult.
In such situation, substitution can usually save the encounter, although at other times, it may be easier to simply scrap the rest of the tale and stick with whatever activities the characters want to engage with.

\section{Hunting}\label{hunting}

Characters hunt by rolling any remotely appropriate Attribute + Ability.
No holds barred, the player simply describes the scenario and rolls the Attributes.
Those attempting to seduce people can roll Charisma + Subterfuge, and cainites who want to sneak into people's people's houses while they sleep can roll Intelligence + Stealth.

Each area comes with a difficulty for hunting there.
The difficulty for the Villages is 8, because people there are superstitious, stay in groups, gossip a lot, and keep their doors barred at night.
The party will not be able to simply `nip off for a drink' with a local.
Anyone leaving with an unknown person (during the night, no less), will be the subject of speculation around the entire village, and anyone attempting to leave will be invited in for food and drink by every old lady within earshot.

Each success grants 2 bloodpoints.
Characters can convert any roll to an automatic murder, worth 10 blood points, in return for a Conscience roll at difficulty 9.

\section{Rumours}\label{rumours}

Characters with dots in Contacts can ask about goings on by spending an hour per die asking for the gossip around town.
Characters with more dice can ask more people, but not every dot needs to be used if time is pressing.
The Storyteller rolls the player's Contacts rating and compares it to the chart below.

The difficulty of any roll should be determined by the area.  Most areas' difficulty will be the same as their `Hunting Difficulty'.

\begin{table}[h]
\begin{tabularx}{\textwidth}{lX}

Botch & Dangerously incorrect information is relayed to the character.  \\

Failure & The character only picks up on propaganda and fantasies.\\

1 success & A basic rumour is found out about one of the previous people known to the characters, or a potential lead on some new resource. \\

2 successes & An interesting rumour is found concerning the current mission.  If none, pick the next encounter for a nearby region and give a rumour about the next encounter there.\\

3 successes & Pick the next encounter immediately and give the player information about it.\\

4 successes & The character receives prime information about enemies involved in the current encounter.\\

5 successes & Vital information about an enemy's weakness spills open.\\

\end{tabularx}\\
\end{table}

Players should not know the results of any roll.
If told `a hideous creature emerges from the river each night', they shouldn't know that this pertains to the Prince but instead wonder if it reflects the usual peasant-cant or a dangerous rumour leading to a watery grave.

\subsubsection{Example Rumours}

If the characters are about to see Jeremiah, they may hear of mad activity among the peasants.
If they have seen the wrath of the Prince, and will soon hear of him again, they might hear of the strange tracks he leaves as he exits the Sava river.
If one of the party has a suitor following them, they will hear of the suitor's family approaching.

