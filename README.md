I have no idea what I'm doing

# Introduction

Belgrade is cool and Vampires are cool, so this is a collection of tabletop RPG stories for Vampire: Dark Ages, set in Belgrade.

# Weird Rules

The character sheets look a little weird because I have house rules.  I'll upload them at some point.

# Encounters: New and Improved!

White Wolf never really did a 'random encounter' mechanic.  So I've added one.  Here's the basic idea: You get two or three simple stories with a part 1, part 2, et c.  When you enter the town, players get a random story/ encounter.  If they've already encountered that story, they move onto part 2, then part 3, et c.

All the stories are made out of stitching random encounters together.

## Example

Once the players enter town, the Storyteller pulls out a random encounter: "The Crone".  The coterie gets tracked down by a dying woman who knows what kind of creatures they are, and requests their blood to survive.  The next time they enter town, they might get Part II where they find the bandits who assaulted her, or they might get a totally different story.

The stories have loose relationships with each other, and sometimes change location.  If you've found a Ravnos tricking the local doctor into bleeding mortals to cure their illnesses, he may skip to the villages and become one of the encounters there.  Players may head towards useful encounters or avoid dangerous ones.  If they've made enemies in town then they might stay away, for fear of their being found out by someone who means them harm.

# Why Belgrade?

Partly, because I live here.  Mostly because Belgrade's history's amazing.  It has:

+ Romans (yes, in 1200 the Romans were still around and owned Serbia)
+ Serbians deciding if they're really a country or not
+ Hungarians gifting all of Belgrade to Serbia
+ The final death of the Roman Empire
+ The kingdom of Serbia wrecking everyone's shit (turns out they really were a country)

The history needs some serious revision, as I'd like to be reasonably historically accurate.  Except for the vampires.

# Basic Plot

The coterie are mass-embraced by a single sire of Clan Toreador.  She is killed for her crimes and the coterie are left to fend for themselves on the worst patch of peasant-land.  Over the coming years, the land becomes more and more Serbian.  The elder vampires, who mostly spoke Greek and Hungarian, lose touch with the common people, and the young coterie gain their edge: they are the only cainites in the area who can really speak to most of the mortals.
